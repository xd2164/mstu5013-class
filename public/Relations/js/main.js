Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

var bookQuery = new Parse.Query('Book');
		bookQuery.descending('published');
		bookQuery.include('likes');

// Query all books
bookQuery.find().then(function(results) {
	// For results, which is an array of books...
	_.each(results, function(book) {
		// For each book do the following...
		console.log(book.toJSON());

		var bookView = new BookView({
			model: book
		});

		$('#books').append(bookView.render().el);
	});
});