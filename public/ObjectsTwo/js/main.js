// Must initialize parse with your app credentials
// App ID , JS Client Key ID
Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

// As soon as this script is loaded, we can start gathering analytics
Parse.Analytics.track('pageLoad', {
	'page': 'objectsTwo'
});

// PARSE DEMO CODE

/*
	Let's create a simple object. A pretty useless object that doesn't have a Class name - but to show.
*/

var myObject = new Parse.Object();

/*
	Remember the new keyword? We're instantiating a new Parse.Object from the Parse.Object class. It's no
	frills and we can't really save it either since Parse wants to know what kind of object it's supposed
	to be. We can create entire sub-classes of specific useful objects for ourselves by defining one.

	Sort of like how we can define classes of javascript objects.
*/

var Video = Parse.Object.extend('Video', {
	defaults: {
		user: undefined,
		director: 'unknown',
		url: 'https://www.youtube.com/watch?v=i2aOgJGJCio',
		title: 'When Cats Attack',
		startTimestamp: 0
	}
});

/*
	Notice the Capital letters for variable and Class name 'Video'. It's a common standard to indicate Classes
	with Capitalization. Of course, JS doesn't care, a variable is a variable. But it makes it much easier for
	developers to identify what is a class and what is an instance.
*/

var video = new Video();

/*
	Based on the class we built, we just created a new instance of the Parse.Object. A Video sub-class of Parse.Object.
	We can create many instances, just like we can with JS objects.
*/

// Parse.User.current() is a way to grab the logged in user from cache.
// In this example, we assume someone is logged in. If they are not, Parse.User.current() = null
var me = Parse.User.current();

var myVideo = new Video();
		// Use two arguments to set one attribute
		myVideo.set('user', me);

		// Use an object and pass in more than one property/value to set multiple attributes
		myVideo.set({
			director: 'Alejandro Veliz',
			url: 'https://lazysomething.com/video'
		});

// Or if you have the data before creating a new instance, you can pass the constructor the attributes
// to populate the new Object from the start.
var yourVideo = new Video({
	director: 'Steven Spielberg',
	url: 'https://www.youtube.com/watch?v=something',
	startTimestamp: 30
});


/*
	In this example, we've so far created a couple Parse Objects that are floating around our client browser memory.
	We can persist this data to our database (DB) using Parse.Object methods.
*/

video.save();

// Saved data have an objectId which Parse assigns on the server. We can check by doing this...

console.log('Video id: ' + video.id);

// or

console.log('Video is new? ' + video.isNew()); // which returns a Boolean true or false. It basically just checks for an objectId.

// The million dollar question is, what will thise two lines output to the log?
// Answer is "Video id: undefined" and "Video is new? false"

/*
	Why?

	We definitely saved the video before checking if it is new.
	This is an example of asynchronicity.

	When video.save() is called, it finishes calling and immediately starts working on executing the next routines.
	It will check for the id and isNew() while the network request is still in progress. This doesn't take long by our
	standards, 300ms or so. But for a computer that is an eternity.

	If you type console.log(video.id); manually right now, it WILL have an id since the request is complete. But at the
	time of our code, in terms of computer time - too late.

	This is why we need callbacks. We need a callback from the save() so that when the request is complete, we execute
	the next routines.

	Parse does this pretty nicely. When you make a network request and are waiting for a response, the method call (save())
	actually returns a promise.
*/

console.log(video.save());

/*
	You'll notice that in the console, we get a weird object back that is indicated as a Promise.
	When Parse makes network requests, it returns a promise to pass the network response to the next thing that is
	designed to accept promises as arguments.

	Let's try this again.
*/

video.save().then(function(response) {	// response is an arbitrary name, could have been named video since that is what will be the response
	console.log(response);
}, function(error) {
	console.log(error);
});

/*
	The video.save() returns a promise that it will pass the response to the .then().

	"I promise that when I get a response, I will pass the response to the next thing to do something with."

	The .then() function has two arguments, both function definitions. First is called a success callback.
	The second is called an error callback. If everything is good, the promise passes the response to the first callback.
	If something goes wrong, an error is the response object that the promise correctly passes to the error callback.

	In this case, we're saving a video object, and the updated video object (one with any new information that the server
	assigned it e.g. objectId, modifiedAt, etc. will be available in the response.) Thus, our console.log(response) will
	execute with the correct, most updated version of the video.
*/

/*
	So if we took the above "trouble" question and recoded it like this...
*/

video.save().then(function(video){
	// This stuff will only happen if the response is successful, and AFTER the request is complete
	console.log('Video id: ' + video.id);						// "Video id: ks93s21s"
	console.log('Video is new? ' + video.isNew());	// "Video is new? false"
}, function(error) {
	console.log(error);
});

console.log('Aardvark');	// This code will run immediately after the .save() call and before the response

// Now we're talking asynchronous and dealing with it correctly.


/*
	In the above code, we basically created some videos and set some values and saved the videos.

	Of course, every time we load this page it's going to create a repeat object with the same data. It'll save()
	and Parse will think it's a new thing and give it a new id and voila. We have a lot of junk demo data.

	That's fine for now. Eventually though, we'll need to learn how to Query data. When we load a page we have no
	objects, how do we get objects from Parse? We'll visit this later.
*/

/*
	The only other thing to think about is how to get data from an object once you have it.

	With Parse objects, we have setters and getters. We've already .set() data. If you want to get a particular
	attribute value from a Parse object, you use the .get() method.
*/

video.get('objectId');
video.get('director');


/*
	We will use Parse objects as MODELS of things we want to represent as VIEWS.
	For example, you could imagine we want to present the video data as some combination of
	video and text information.

	For now, we can just practice using the Parse Objects and directly using jQuery to throw that information
	into the webpage.

	But eventually, we'll see why this way of doing things isn't very efficient and we might want to create
	better ways of managing how Objects (particularly when we have many of them) can be represented visually.
*/
var $demoH1 = $('<h1>');
		$demoH1.html('Video director is: ' + video.get('director'));
$('body').html($demoH1);
