Parse.initialize("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ");


// One option is to do a query first. THEN mount the todo-app with the requisite data.
var query = new Parse.Query("Todo");

query.find().then(function(todos) {

	// RIOT MOUNT
	riot.mount('todo-app', {
		user: {
			first: "Adam",
			last: "Smith"
		}, // Once we learn about Parse.User we can include that too. For now, simple JS Object {}
		todoList: todos
	});

});