<todo-app>

	<div class="todoapp">
		<h3>Hello { opts.user.first } { user.last }! </h3>

		<form>
			<div class="form-group">
				<label for="todoInput">Enter your todo</label>
				<input type="text" class="form-control" onkeypress={ addItem }>
			</div>
		</form>

		<h3>You have { todoList.length } items todo</h3>
		<ol>
			<todo-item each={ todoModel in todoList }></todo-item>
		</ol>
	</div>

	<div class="zoo">
		<h3>Zoo</h3>
		<ul>
			<button onclick={ fetchAnimals }>Fetch Animals</button>

			<div class="zooItems">
				<animal each={ animal in animalList }></animal>	
			</div>
			
		</ul>
	</div>

	<script>

		this.user = this.opts.user;
		this.todoList = this.opts.todoList

		// We're going to do a Parse.Query to get this one here...
		this.animalList = [];

		this.addItem = function(event) {
			// console.log('event.item', event.item);
			if (event.which === 13) {

				var dateInOneWeek = new Date();
						dateInOneWeek.setDate(dateInOneWeek.getDate() + 7);

				// Or we can just define this globally in main.js
				var Todo = Parse.Object.extend('Todo');
				var todo = new Todo();
				todo.set({
					task: event.currentTarget.value,
					dueBy: dateInOneWeek,
					done: false
				});

				this.todoList.push(todo);

				todo.save();

				event.currentTarget.value = "";

			} else {
				return true;
			}
		};

		this.on('update', function() {

			// Sort by closest due date first
			this.todoList.sort(function(a, b) {
				return a.get('dueBy').getTime() - b.get('dueBy').getTime();
			});
			
		});

		this.fetchAnimals = function(event) {
			// In Riot event handlers, 'this' automatically points/binds to this component.
			var that = this;

			// But inside Parse callbacks, 'this' is not the component (actually points to the promise) -> So we use 'that' which is = to this component;
			var query = new Parse.Query('Animal');
					query.find().then(function(animals) {
						console.log('This & That', this, that);

						that.animalList = animals;

						/* You'll find that pushing the button does make the query. And this.animalList is now set to the array of Animal Parse.Objects. However, remember that queries are async. And this component update() happens immediately after the this.fetchAnimals callback is executed. So, the response does not return before the update(). That's why we don't see Animal components. In this case, we need to manually call this.update() */

						that.update(); // Try commenting this out to see the component load fail.
					});
		};

	</script>

	<style>
		.todoapp {
			background-color: orange;
			padding: 30px;
		}
		.zooItems {
			margin-top: 15px;
		}
	</style>
</todo-app>
