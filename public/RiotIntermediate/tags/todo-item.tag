<todo-item>
	<li class={ completed:data.done }>
		{ data.task }
		<button onClick={ toggleDone }>Done</button>
		<button onclick={ removeItem }>Destroy</button>
		<span onclick={ openDateEditor } show={ !editingDate }>{ dueDate }</span>
		<input name="dueDateInput" type="date" if={ editingDate } onchange={ updateDueBy }>
	</li>

	<script>
		var that = this;
		// console.log('todo-item:', this);

		// Make a copy of the model's (Parse.Object's) attributes
		this.data = this.todoModel.toJSON();
		this.dueDate = formatDate(this.todoModel.get('dueBy'));

		function formatDate(date) {
			var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
			var formatted = "DUE: " + months[date.getMonth()] + " " + date.getDate();
			return formatted;
		}

		// I'm keeping these states which concern the view but NOT the model directly, separate.
		this.editingDate = false;
		this.editingTask = false;


		this.openDateEditor = function(event) {
			this.editingDate = !this.editingDate;
		};
		this.updateDueBy = function(event) {

			var dateStr = event.currentTarget.value; // E.g. 2016-03-23
			var dateSplit = dateStr.split('-'); // E.g. ['2016', '03', '23'];

			var date = new Date();
					date.setYear(dateSplit[0]);
					date.setMonth(Number(dateSplit[1]) - 1);
					date.setDate(dateSplit[2]);

			this.todoModel.set('dueBy', date);
			this.todoModel.save();

			this.openDateEditor();
		};

		// Because todoModel.toJSON() returns a copy, every time we update and before we rerender the HTML, we need to compute the newest most updated source of truth as this.data
		// This is because we are not manipulating this.data directly. We are manipulating the todoModel / Parse.Object through it's setters and getters. Setting todoModel.set('done', true) does not change this.data.done. But if we syc this.data to the current state / source of truth of the todoModel by using todoModel.toJSON(), our data is always in sync before rendering.
		this.on('update', function() {
			this.data = this.todoModel.toJSON();
			this.dueDate = formatDate(this.todoModel.get('dueBy'));
		});

		this.toggleDone = function(event) {
			this.todoModel.set('done', !this.data.done);

			// Here we are saving the updated 'done' status
			this.todoModel.save();
		};

		this.removeItem = function(event) {
			var index = this.todoList.indexOf(this.todoModel);
			this.todoList.splice(index, 1);
			this.todoModel.destroy();
		}

	</script>

	<style scoped>
		
		:scope {
			font-family: "Times New Roman";
		}		

		li {
			background-color: pink;
			padding: 10px;
			margin-bottom: 10px;
		}

		.completed {
			text-decoration: line-through;
			color: silver;
			font-style: italic;
		}
	</style>
</todo-item>