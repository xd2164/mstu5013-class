<animal>
	
	<div class={ animal:true, animalM:(data.sex=='male'), animalF:(data.sex=='female') }>
		<h3>{ data.animal } | <small>{ data.type }</small></h3>
		<div>
			OTHER INFO: <span each={ label, i in data.other }>{ label } { i < data.other.length - 1 ? '~ ' : '' }</span>
		</div>
	</div>

	<script>
		this.data = this.animal.toJSON();
	</script>
	
	<!-- This scoped attribute in the style tag limits the scope of the CSS rules to just this component -->
	<style scoped>

		/* :scope is a special CSS selector ONLY available in RiotJS. It defines rules to be applied to the component tag <animal> but scoped within this component. It only works if you have the 'scoped' attribute in the above <style> tag. */
		:scope {
			display: block;
			background-color: #FFFFFF;
			margin-bottom: 10px;
		};
		.animal {
			color: #FF0044;
			padding: 0 15px 15px;
			border-radius: 8px;
		}
		.animalM {
			border: 8px solid #FF9800;
		}
		.animalF {
			border: 8px solid #8BC34A;
		}
	</style>
</animal>