// This file is for objects and UI

// Event listener for save button
$('#saveAnimal').click(saveAnimal);

// Function to save animal
function saveAnimal() {

	// Converting any case into lowercase
	var animal = $('#animal').val().toLowerCase() || undefined;

	var type = $('#type').val() || undefined;
	var sex = $('[name="sex"]:checked').val() || null;
	
	// Remember, HTML val will produce strings
	// We cast the string into a Number data type
	var quantity = Number($('#quantity').val());
	
	// If there is a string representation of a date e.g. 2016-01-01, convert to a Date object
	var acquired = $('#acquired').val() ? new Date($('#acquired').val()) : undefined;

	// Selecting the checked inputs
	// Using jQuery map function to get the value of each checked input
	// using the jQuery toArray() to push those values to an []
	var other = $('#other input:checked').map(function(){
		return $(this).val();
	}).toArray();

	// We MUST have an animal name, so we do a check before saving
	if (animal) {
		
		var myAnimal = new Animal({
			animal: animal,
			type: type,
			sex: sex,
			quantity: quantity,
			acquiredAt: acquired,
			other: other
		});

		myAnimal.save({
			success: function(){
				showFeedback(true, 'Object successfully saved');
			},
			error: function(){
				showFeedback(false, error.message);
			}
		});

		// BETTER WAY is to use the Promise form
		// .then() is a Promise, to be covered later

		// myAnimal.save().then(function(){
		// 	showFeedback(true, 'Object successfully saved');
		// }, function(error){
		// 	showFeedback(false, error.message);
		// });

		resetForm();

	} else {
		showFeedback(false, 'You must declare an Animal name.');
	}
}

// Form RESET function
function resetForm() {
	$('input[type="text"]').val('');
	$('select').val('');
	$('#quantity').val(1);
	$('[type="checkbox"]:checked').attr('checked', false);
	$('input[name="sex"]').prop('checked', false);
	$('#sex-n').prop('checked', true);

	$('#animal').select();
}