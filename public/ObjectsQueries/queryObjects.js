$('#getById').click(getById);
$('#getBySex').click(getBySex);

$('#getLess, #getGreater').click(getByQuantity);
$('#getBefore, #getAfter').click(getByAcquired);
$('#getOtherAnd, #getOtherOr').click(getByOther);

// Query by ID
function getById() {
	var id = $('#objectId').val();

	var query = new Parse.Query('Animal');
			query.equalTo('objectId', id);

	// Since objectId is unique, I know I will only
	// have at most 1, so I can use the .first()
	// method instead of .find() to return just one object
	query.first().then(function(foundAnimal) {
		console.log(foundAnimal);
		printAnimals(foundAnimal);


		if (foundAnimal) {
			showFeedback(true, 'Animal Found');
		} else {
			showFeedback(false, 'Animal Not Found');
		}
		
	}, function(error){
		showFeedback(false, error.message);
	});
}

function getBySex() {
	var sex = $('#querySex').val();

	if (sex === "") {
		sex = null;
	}

	var query = new Parse.Query('Animal');
			query.equalTo('sex', sex);

	// Results will be an array of animals
	// Maybe an empty array if there are no animals
	query.find().then(function(results){

		var count = results.length;

		printAnimals(results);

		if (results.length > 0) {
			showFeedback(true, count + ' Animals Found');
		} else {
			showFeedback(false, 'Animals Not Found');
		}
	}, function(error){
		showFeedback(false, error.message);
	});

}

function getByQuantity() {
	// Convert the string number to a real Number
	var quantity = Number($('#queryQuantity').val());
	
	var query = new Parse.Query('Animal');

	if ($(this).attr('id') === 'getLess') {
		query.lessThan('quantity', quantity);
	} else {
		query.greaterThan('quantity', quantity);
	}

	query.find().then(function(results){
		var count = results.length;

		printAnimals(results);

		if (results.length > 0) {
			showFeedback(true, count + ' Animals Found');
		} else {
			showFeedback(false, 'Animals Not Found');
		}
	}, function(error){
		showFeedback(false, error.message);
	});
}

function getByAcquired() {
	// Convert the string date to a date Object
	var date = new Date($('#queryAcquired').val());
	
	console.log(date);

	var query = new Parse.Query('Animal');

	if ($(this).attr('id') === 'getBefore') {
		query.lessThan('acquiredAt', date);
	} else {
		query.greaterThan('acquiredAt', date);
	}

	query.find().then(function(results){
		printAnimals(results);

		var count = results.length;

		if (results.length > 0) {
			showFeedback(true, count + ' Animals Found');
		} else {
			showFeedback(false, 'Animals Not Found');
		}
	}, function(error){
		showFeedback(false, error.message);
	});
}

function getByOther() {

	var pushedButtonId = $(this).attr('id');

	var other = $('#queryOther input:checked').map(function(){
		return $(this).val();
	}).toArray();

	var query = new Parse.Query('Animal');

	if (pushedButtonId === 'getOtherAnd') {
		query.containsAll('other', other);
	} else {
		// getOtherOr
		query.containedIn('other', other);
	}

	query.find().then(function(results){
		printAnimals(results);

		var count = results.length;

		if (results.length > 0) {
			showFeedback(true, count + ' Animals Found');
		} else {
			showFeedback(false, 'Animals Not Found');
		}
	}, function(error){
		showFeedback(false, error.message);
	});

}



// Accepts array of animal objects
// OR a single animal object which it puts in an array
function printAnimals(animals) {

	if (animals) {
		animals = animals.constructor !== Array ? [animals] : animals;

		// The following code simply outputs each object in the results []
		// to the <pre> section in a formatted fashion.
		var jsonTxt = "";

		for (var i = 0; i < animals.length; i++) {
			jsonTxt += '<pre>';
			jsonTxt += JSON.stringify(animals[i].toJSON(), null, 2);
			jsonTxt += '</pre>';
			// jsonTxt += i === animals.length - 1 ? '' : '\n\n';
		}

		$('#zooList').html(jsonTxt);
	} else {
		// CLEAR list
		$('#zooList').html('');
	}
}


// Kick off the page with all animals
function getAllAnimals() {
	
	var query = new Parse.Query('Animal');

	query.find().then(function(results) {

		printAnimals(results);

	});
}

// Notice the page is instantly populated with objects
// This function kicks in to start things.
getAllAnimals();









/* QUERYING more than one field at a time... Just add more query constraints */

$('#compSearch').click(function(event) {
	var compQuery = new Parse.Query('Animal');
	
	var sex = $('#compSex').val();
	var other = $('#compOther input:checked').map(function(event) {
		return $(this).val();
	}).toArray();

	// Only need to add this query constraint if something is chosen
	if (sex) {
		compQuery.equalTo('sex', sex);
	}

	// Only need to add this query constraint if something is chosen
	if (other.length > 0) {
		compQuery.containedIn('other', other);
	}

	// Only do a search if some constraint is chosen
	if (sex || other.length > 0) {
		compQuery.find().then(function(results) {
			printAnimals(results);
			showFeedback(true, results.length + " animals found.");
		});		
	} else {
		showFeedback(false, 'Please narrow your query parameters.');
	}


});
