Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

// Here we create a new Navbar view
var navbar = new Navbar({
	// We can pass in an id to tell this View what the root element (el)
	// should be. Useful if your el already exists and we simply want
	// to attach the View object to that existing root element.
	el: '#navigation'
});

// All code related to the editor view goes here
var jokeEditor = new JokeEditor({
	el: '#jokeEditor'
});

var lastChecked;
var jokeQuery = new Parse.Query(Joke);

fetchJokes();

function fetchJokes() {
	jokeQuery.find().then(function(jokes){
		_.each(jokes, function(joke) {
			var jokeView = new JokeView({
				model: joke
			});
			$('#jokeList').prepend(jokeView.render().el);
		});
	});

	lastChecked = new Date();
	jokeQuery.greaterThan('createdAt', lastChecked);

}