var JokeView = Parse.View.extend({
	events: {
		'click .joke-content': 'revealPunchline',
		'click .funny': 'addFunny',
		'click .notFunny': 'subtractFunny'
	},
	initialize: function() {

	},
	render: function() {
		var tmp = $('#jokeTMP').html();
		var compiled = _.template(tmp);

		this.$el.html(compiled(this.model.toJSON()));

		return this;
	},
	revealPunchline: function() {
		this.$('.punchline').removeClass('hidden');
	},
	addFunny: function() {
		this.model.increment('score');
		this.model.save();
		this.render();
	},
	subtractFunny: function() {
		this.model.increment('score', -1);
		this.model.save();

		if (this.model.get('score') < -3) {
			this.model.destroy();
			this.remove();
		} else {
			this.render();	
		}
	}
});