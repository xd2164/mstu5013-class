var JokeEditor = Parse.View.extend({
	events: {
		'click #submitJoke': 'submitJoke'
	},
	initialize: function() {
		console.log('jokeEditor initialized');
	},
	submitJoke: function() {
		var author = this.$('#author').val();
		var joke = this.$('#joke').val();
		var punchline = this.$('#punchline').val();

		var jokeObject = new Joke({
			author: author ? author : 'anonymous',
			joke: joke ? joke : 'No joke, is that the joke?',
			punchline: punchline ? punchline : null
		});

		jokeObject.save().then(function(){
			fetchJokes();
		});

		this.resetForm();

	},
	resetForm: function() {
		this.$('input').not('#author').val('');
	}
});