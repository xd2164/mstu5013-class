Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

if (Parse.User.current()) {
	$('#loginMsg').html('こんにちは ' + Parse.User.current().get('first') + ' | <a id="logout" href="./">logout</a>');
}

$('#logout').click(function(event) {
	Parse.User.logOut();
});

// Create a custom sub-class of Parse.Object which is our Message object
var Message = Parse.Object.extend('Message', {
	defaults: {
		content: undefined,
		tag: 'normal'				// Set default values...
	},
	initialize: function() {
		console.log('Message Initialized');
	}
});


$('#sendMessage').click(function(event) {

	if (Parse.User.current()) {

		// Create an instance of a new Message
		var message = new Message();

			message.set({
				content: $('#message').val(),
				tag: $('[name="priority"]:checked').val(),
				user: Parse.User.current()
			});

		message.save().then(function(arg) {
			alert('Message Saved');
		}, function(error) {
			alert(error.message);
		});

		resetForm();

	} else {
		alert('You are not logged in.');
	}

});

// QUERY - get past messages

var allMessages;

var myQuery = new Parse.Query('Message');

		myQuery.descending('createdAt');
		myQuery.equalTo('user', Parse.User.current());
		// myQuery.notEqualTo('user', Parse.User.current());
		// myQuery.limit(2);
		// myQuery.skip(2);

	myQuery.find().then(function(results){

		console.log(results);

		for (var i=0; i < results.length; i++) {
			var $li = $('<li>');
					$li.prepend('<span class="tag">' + results[i].get('tag') + '</span>');
					$li.find('.tag').addClass(results[i].get('tag') === 'high' ? 'high' : 'normal');
					$li.append(results[i].get('content'));

			$('ul').append($li);
		}

	}, function(){

	});





$('#refresh').click(function(event) {
	// QUERY
});


function resetForm() {
	$('[name="priority"]').attr('checked', false).filter('[value="normal"]').prop('checked',true);
	$('#message').val('').focus();
}









