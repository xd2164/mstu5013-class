<recipe-editor>
	<div class="editor">
		<span>Create demo recipe</span>
		<button onclick={ saveRecipe }>Create</button>
	</div>

	<script>
		var Recipe = Parse.Object.extend('Recipe');



		this.saveRecipe = function(event) {

			// For DEMO purposes only, I'm creating users from the DB without a query
			var user1 = new Parse.User();
					user1.id = 'RLRnkBZqmI';
			var user2 = new Parse.User();
					user2.id = 'xinQSPx93m';

			// I'm just generating a new recipe with various datatypes as example
			var myRecipe = new Recipe({
				easy: true,
				ingredients: [
					{ material:'apple', quantity: '4'},
					{ material:'sugar', quantity: '1 cup'},
					{ material:'cinnamon', quantity: '1 tbs'}
				],
				instructions: 'Core the apples. Mix sugar and cinnamon. Put sugar mix in apple core hole. Bake for 30 min at 300 degrees F.',
				keywords: ['beginner', 'popular'],
				likes: [user1, user2],
				name: 'Sweet Apple Popper',
				servings: 4
			});

			myRecipe.save();
		}
	</script>
</recipe-editor>