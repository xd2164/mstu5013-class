<recipeA>
	<li>
		<div>
			<span>This is { data.easy ? 'very' : 'not'} easy.</span>
			<button onclick={ toggleEasy }>Toggle easy</button>
		</div>
		<div>
			<span>Click to save state</span>
			<button onclick={ saveRecipe }>Save</button>
		</div>
		<div>
			<span>Click to destroy</span>
			<button onclick={ deleteRecipe }>Destroy</button>
		</div>
	</li>

	<script>
		// Don't be afraid to use console.log() to see what's happening
		console.log('TAG: recipe', this);
		console.log('opts: recipe', this.opts);
		window.x = this;

		// console.log(this.opts.parseObj); // No good >>> riot lowercases parseObj to parseobj. See cookbook.tag for details
		// console.log(this.opts.parseobj);

		// this.sourceOfTruth = this.opts.parseobj;
		// this.data = this.sourceOfTruth.toJSON();

		// console.log(this.sourceOfTruth.toJSON());

		// // Event Cycle
		// this.on('update', function() {
		// 	// Every time this component 'updates'
		// 	// We recompute what this.data is from the
		// 	// Parse.Object.toJSON() function
		// 	// this.data is what the dynamic HTML uses as reference
		// 	this.data = this.recipe.toJSON();
		// });


		// /* Functions */
		// this.toggleEasy = function(event) {
		// 	var easy = this.recipe.get('easy');

		// 	this.recipe.set('easy', !easy);
		// };

		// this.saveRecipe = function(event) {
		// 	console.log('convenience shortcut', event.item);
		// 	this.recipe.save();
		// };

		// this.deleteRecipe = function(event) {

		// 	// Destroy this.recipe -> our Parse.Object
		// 	this.recipe.destroy();
			
		// 	// But, destroying this.recipe alone won't remove it from view.
		// 	// Remember, destroy() is just a command to remove it from the DB
		// 	// But our recipeList array in the parent still refers to it locally
		// 	// So it remains in the array... and in our visual list

		// 	// Now we're basically REMOVING it from the parent recipeList
		// 	// This will update the parent, then update the recipe children
		// 	// Since the recipeList has 1 less, we have one less child component
		// 	var index = this.parent.recipeList.indexOf(this.recipe);
		// 	this.parent.recipeList.splice(index, 1);
		// };
	</script>

	<style scoped>
		li {
			color: red;
		}
	</style>

</recipeA>