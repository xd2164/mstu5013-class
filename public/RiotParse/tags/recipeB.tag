<recipeB>
	<li>
		<div>
			<span>This is { data.easy ? 'very' : 'not'} easy.</span>
			<button onclick={ toggleEasy }>Toggle easy</button>
		</div>
		<div>
			<span>Click to save state</span>
			<button onclick={ saveRecipe }>Save</button>
		</div>
		<div>
			<span>Click to destroy</span>
			<button onclick={ deleteRecipe }>Destroy</button>
		</div>
	</li>

	<script>
		// Don't be afraid to use console.log() to see what's happening
		console.log('TAG: recipe', this);
		console.log('opts: recipe', this.opts);

		// The source of truth, is the Parse.Object that is passed into this component
		// as this.recipe (automatically attached to this component through each={ in }) directive. You'll often here me call it the model for our view or our data. Through the each={ in } we also have access to the recipeList in the parent through this.recipeList

		// It's easier to work with simple objects. Parse.Objects have a convenient function called Parse.Object.toJSON(). It takes the complex Parse.Object and returns a copy of a simple javascript object with only the Parse.Object attributes included. The returning copy from toJSON() is NOT the parse object. Just a copy of it. Useful. But manually changing the copy will not alter the original Parse.Object. We need to use the Parse.Object.set() methods to do that.
		console.log(this.recipe.toJSON());
		this.data = this.recipe.toJSON();

		// Since this.recipe.toJSON() is just a copy, it also won't change automatically if we actually change the original Parse.Object. So, somehow every time we update this component, we need to make sure our data represents the state of the actual Parse.Object. Our model data. That is our source of truth. Notice the next function call.

		// Event Cycle
		// Riot has certain events that trigger thoughout the lifetime of a tag/component. One is update. It is an event that is triggered every time Riot automatically calls an .update() function on the component or if one manually calls it. We can listen to this (much like we can listen to a click or keypress). The following is saying, this component should listen for the 'update' event. When it is triggered, it will run the callback function. The callback function, updates this.data to get a new, most up to date, source of truth copy of the attributes in the Parse.Object.
		this.on('update', function() {
			this.data = this.recipe.toJSON();
			// Thus, on each update we compute what the latest state of the Parse.Object is. Then we set this.data to that information. After update, the component will rerender itself (with the newest data).
		});


		/* Functions */
		this.toggleEasy = function(event) {
			var easy = this.recipe.get('easy');

			this.recipe.set('easy', !easy);
		};

		this.saveRecipe = function(event) {
			console.log('convenience shortcut', event.item);
			this.recipe.save();
		};

		this.deleteRecipe = function(event) {

			// Destroy this.recipe -> our Parse.Object
			this.recipe.destroy();
			
			// But, destroying this.recipe alone won't remove it from view.
			// Remember, destroy() is just a command to remove it from the DB
			// But our recipeList array in the parent still refers to it locally
			// So it remains in the array... and in our visual list

			// Now we're basically REMOVING it from the parent recipeList
			// This will update the parent, then update the recipe children
			// Since the recipeList has 1 less, we have one less child component
			var index = this.parent.recipeList.indexOf(this.recipe);
			this.parent.recipeList.splice(index, 1);
		};
	</script>

	<style scoped>
		li {
			color: blue;
		}
	</style>

</recipeB>