<cookbook>
	<!-- You can arbitrarily use JS in these {} -->
	<h1>{ greeting } { opts.user.first } to the cookbook!</h1>
	
	<!-- Convention prefer hypen separated (kebab style) tag names. -->
	<!-- This is one child component -->
	<recipe-editor></recipe-editor>

	<ol>
		<!-- recipeList means this.recipeList -->
		<!-- You have access to properties of this component through just the { propertyName } -->
		<!-- each={ recipeList } passes a shallow copy of each recipe -->
		<!-- this is problematic because Parse.Object attributes are nested -->
		<!-- inside the Parse.Object (E.g. Parse.Object.attributes) and not at the shallow surface -->
		<!-- <recipeA each={ recipeList } data={ this }></recipeA> -->
		<!-- data={this} also gives only a shallow copy to the child through opts.data -->

	</ol>

	<ol>
		<!-- Notice the difference (in console) between above and each={recipe in recipeList} -->
		<recipeB each={ recipe in recipeList }></recipeB>
		<!-- This is useful because we can be sure that the full Parse.Object -->
		<!-- is passed to the child. This is automatically attached to the child component as child.recipe and NOT passed through this.opts.recipe -->
		<!-- For us, it's convenient to work with the full Parse.Object so for now, unless I see some major disadvantage to this, this is a more preferrable way to deal with using loops and passing complex objects to children like Parse.Object -->
	</ol>
	

	<script>

		// Don't be afriad to use console.log() to see what's happening
		console.log('TAG: cookbook', this);

		// Notice the object doesn't directly have properties...
		// the properties are in the prototype
		console.log('opts: cookbook', this.opts);

		// When we mouted riot, we threw in an object like this
		// { recipeList: queryResults }
		// We can find them in this component's options this.opts
		// As a shortcut, I'm going to refer to this.opts.recipeList here.
		this.recipeList = this.opts.recipeList;

		// Random greeting
		this.greeting = (Math.random() > 0.5) ? 'Welcome' : 'Bienvenudo';

		// Notice in greeting I have opts.user.first
		// It's long winded. Could have been shortened to { user.first }
		// If I just did this.user = this.opts.user
		// Take advantage to creating temporary references in "this" context.

		this.me = this.opts.user;

	</script>

</cookbook>