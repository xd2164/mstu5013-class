Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

// Instead of having an array of todoItems...
// We're going to use a superhero array like object...
// A Parse Collection to hold our todoItems

// Defining a Collection
var TodoCollection = Parse.Collection.extend({
	model: Todo, // model tells what models this collection is of
	
	// We can set the collection to have a
	// default query that represents the collection
	query: (new Parse.Query(Todo)).descending('createdAt')
});

var todoCollection = new TodoCollection();


var app = new AppView({
	el: '#app', // Need the document ready for this to be there when we need it

	// We can pass in a collection as an option
	// much like we can pass in a model.
	// Inside the AppView we refer to the todoCollection as
	// this.collection (notice the property name)
	collection: todoCollection
});

// Fetch and reset the initial collection
todoCollection.fetch();



