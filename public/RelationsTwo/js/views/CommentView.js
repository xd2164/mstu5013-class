var CommentView = Parse.View.extend({
	className: 'comment',
	template: _.template($('#commentTMP').html()),
	render: function() {
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}
});