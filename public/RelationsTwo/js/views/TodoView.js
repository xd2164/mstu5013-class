var TodoView = Parse.View.extend({
	tagName: 'li',
	className: 'todoItem',
	template: _.template($('#todoItemTMP').html()),
	events: {
		'click .makeComment': 'makeComment',
		'click .fa-comment-o': 'toggleComment',
		'click .fa-check': 'toggleComplete',
		'click .fa-trash': 'deleteTodo'
	},
	initialize: function() {
		this.model.on('sync', this.render, this);
		this.model.on('destroy', this.remove, this);
	},
	render: function() {
		
		var data = this.model.toJSON();

		this.$el.html(this.template(data));

		if (this.model.get('comments').length > 0) {
			var recent = this.model.get('recentComment');
			this.$('.commentList').html('<span class="badge">' + this.model.get('commentCount') + '</span>&nbsp;&nbsp;' + recent.content);
		} else {
			this.$('.commentList').html('<em>No Comments</em>');
		}

		if (this.model.get('status') === 'complete') {
			this.$('.todoContent').addClass('complete');
		}

		return this;
	},
	makeComment: function() {
		var fullName = Parse.User.current().get('first') + ' ' + Parse.User.current().get('last');

		var commentText = this.$('.commentInput').val();
		var comment = new Comment({
			user: Parse.User.current(),
			author: fullName,
			content: commentText
		});

		this.model.add('comments', comment);
		this.model.set('recentComment', {
			author: fullName,
			content: commentText
		});
		this.model.set('commentCount', this.model.get('comments').length);

		this.model.save();

		this.$('.commentInput').val('');
	},
	toggleComment: function() {
		var that = this;

		if (this.$('.commentEditor').hasClass('hidden')) {
			// Fetch the rest of the comments
			Parse.Object.fetchAll(this.model.get('comments')).then(function(results) {

				that.$('.commentList').html('');

				_.each(results, function(comment) {
					var commentView = new CommentView({
						model: comment
					});
					that.$('.commentList').append(commentView.render().el);
				});

			});
		}

		this.$('.commentEditor').toggleClass('hidden');
	},
	toggleComplete: function() {
		if (this.model.get('status') === 'incomplete') {
			this.model.set('status', 'complete');
		} else {
			this.model.set('status', 'incomplete');
		}

		this.model.save();
	},
	deleteTodo: function() {
		var nestedComments = this.model.get('comments');

		this.model.destroy();
		Parse.Object.destroyAll(nestedComments);
	}
});