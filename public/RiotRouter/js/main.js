// Parse.initialize("AppKey", "JSKey");



riot.mount('app');



/* ROUTES */

riot.route('/breads', function() {
	// mount <breads> to <div id="page">
	// <div id="page"> becomes <div id="page" riot-tag="breads">
	riot.mount('#page', 'breads');
});

riot.route('/breads/*', function(id) {
	riot.mount('#page', 'breads', {
		objectId: id
	});
});

riot.route('/cookies', function() {
	riot.mount('#page', 'cookies');
});

riot.route('/cakes', function() {
	riot.mount('#page', 'cakes');
});

riot.route(function() {
	riot.mount('#page', 'breads');
});

riot.route.start(); // See app.tag -> riot.router.exec()
// We execute the mount in the app.on('mount') event handler to make sure that #page
// which exists in the app component is DOM ready for routing.