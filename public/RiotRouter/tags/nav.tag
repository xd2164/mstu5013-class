<nav>
	<a href="#">HOME</a>
	<a href="#breads">#BREADS</a>
	<a href="#breads/123">#BREADS/objectId</a>
	<a href="#cookies">#COOKIES</a>
	<a href="#cakes">#CAKES</a>

	<style scoped>
		a:not(:last-of-type) {
			margin-right: 15px;
		}
	</style>
	
</nav>