<app>
	
	<div>APP COMPONENT</div>
	<nav></nav>
	
	<!-- DIRECTLY mounting to a existing element (see Router) -->
	<div id="page"></div>

	<script>
		this.on('mount', function() {
			console.log('app mount');
			riot.route.exec();
		});
	</script>

	<style scoped>
	</style>

</app>