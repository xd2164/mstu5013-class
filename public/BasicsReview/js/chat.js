// INITIALIZE PARSE
Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

// Check if they are NOT logged in...
if (!Parse.User.current()) {
	// And redirect them if that is the case
	window.location.href = './index.html';
}

// Define our ChatMessage Object
var ChatMessage = Parse.Object.extend('ChatMessage', {
	defaults: {
		'message': ''
	}
});

/* INITIAL CODE TO SETUP SOME GLOBALS */

// Keeps track of last time we fetched
var lastMessageDate = new Date();	// Date Object / timestamp
// Subtract 7 days from date so we can start with some old messages
lastMessageDate.setDate(lastMessageDate.getDate() - 7);

// We will set a query to grab chatMessages
var chatQuery = new Parse.Query(ChatMessage);
chatQuery.include('user'); // include nested user object
// Most recent chatMessage will be the last in our query result []s
chatQuery.ascending('createdAt');

// First call immediately on page load
fetchMessages();

// Then, set a timer and fetch messages automatically
// every 5 seconds for a real time chat experience.
// Usually you would do something like a deteriorating timer....
// Every 1s after they type or click on something, then 3s, then 5s,
// then 10s, then 15s, to account for user activity and to save
// network calls.
setInterval(fetchMessages, 5000); // 5000ms = 5s


/* UI CODE */

// Message Input
$('#message').keyup(function(event){
	if (event.which === 13) {
		var message = $('#message').val();

		sendMessage(message);

		$(this).val('');
	}
});

$('#logout').click(function(event) {
	Parse.User.logOut();
	window.location.href = './index.html';
});


/* FUNCTIONS TO KEEP THINGS CLEAN AND SEPARATED */

// Function to send message to parse
function sendMessage(message) {
	// Send to Parse
	var chatter = new ChatMessage({
		'user': Parse.User.current(),	// So each message is associated to a user
		'message': message
	});

	chatter.save().then(function(msg){
		console.log('message saved');
	});
}

// Function to print message to chat
// Takes ChatMessage Parse.Object as argument
function printMessage(messageObj) {
	// Create <li>
	var $li = $('<li>');
	// Concat user name to front of message
	message = messageObj.get('user').get('first') + ': ' + messageObj.get('message');
	// Populate li with message
	$li.html(message);
	// Append li to chat list
	$('#chat').append($li);
}

// Fetch messages createdAt AFTER timestamp which represents
// the last time we fetched messages. In other words,
// get messages saved since the last time we fetched.
function fetchMessages() {
	// Update the query
	chatQuery.greaterThan('createdAt', lastMessageDate);

	// Find the messages
	chatQuery.find().then(function(messages) {
		// It's an [] of messages so loop through and append each
		for (var i=0; i < messages.length; i++) {
			// Remember, it is an [] of Parse.Objects... so make sure
			// printMessage() takes Parse.Objects as arguments
			printMessage(messages[i]);

			// If it is the last (most recent) message...
			if (i === messages.length - 1) {
				// Save the date as new last message timestamp
				lastMessageDate = messages[i].createdAt;
			}
		}
	}, function(error){
		console.log(error);
	});
}







