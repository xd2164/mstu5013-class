// INITIALIZE PARSE
Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");


$('#register').click(function(event){
	var username = $('#register-email').val();
	var email = $('#register-email').val();
	var password = $('#register-password').val();
	var first = $('#register-first').val();
	var last = $('#register-last').val();

	// Instance way of signUp
	var user = new Parse.User();
			user.set('username', username);
			user.set('password', password);
			user.set('email', email);

	// With the user instance setup... signUp the user
	user.signUp({
		first: first,
		last: last,
		type: 'beta-tester'
	}, {
		// Backbone style async callbacks
		success: function(user) {
			window.location.href = "./chatroom.html";
		},
		error: function(error) {
			console.log(error);
		}
	});

	// I prefer the Static Class way - it's shorter
	// And I like promise style callbacks to Backbone style
	/*
		Below pseudocode does the same thing as above...

		Parse.User.signUp(username, password, {...}).then(function(user) {
			window.location.href = ...
		}, function(error) {
			... // some error code
		});

	*/
});

$('#login').click(function(event) {
	var username = $('#login-username').val();
	var password = $('#login-password').val();

	// Static logIn() and Promise style async callbacks - cleaner
	Parse.User.logIn(username, password).then(function(arg) {
		console.log('success');
		window.location.href = "./chatroom.html";
	}, function(error) {
		console.log(error);
	});
});