Parse.initialize("8shSBt0nS2avoZGq1G9atK7s6QQTh2KpEeNAPWR4", "Syk8Nz51z5GaXqIeZyJOgl0cfRYb4H3Db7aOQO13");

var me = Parse.User.current();

// var X = Parse.Object.extend(...);

var UserView = Parse.View.extend({
	events: {
		'click .tag': 'onClickTag'
	},
	initialize: function() {
		console.log('view is initialized');
	},
	render: function() {
		// grab the template
		var tmp = $('#userTemplate').html();

		// plug in the data into the template
		var compiled = _.template(tmp);
		
		var userData = this.model.toJSON();

		var HTMLstring = compiled(userData);

		this.$el.html(HTMLstring);

		return this;

	},
	onClickTag: function() {
		alert(this.model.get('first') + ': tag was clicked');

		if (this.model.get('tag') === 'friend') {
			this.model.set('tag', 'enemy');
		} else {
			this.model.set('tag', 'friend');
		}

		this.model.save();
	}
});



var darth = new Parse.User({
	first: 'Darth',
	last: 'Vader',
	tag: 'Evil',
	bio: 'Father of Luke and Leia'
});

var users = [me, darth];

for (var i=0; i < users.length; i++) {
  var user = users[i];
  var userView = new UserView({
    model: user
  });
  userView.render();
  $('body').append(userView.el);
}