<many-to-many-large>
	<div class="page-header">
		<h3>MANY-TO-MANY LARGE <small>Parse.Relations</small></h3>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h3>Books <small>Using copy of Parse Object</small></h3>
			<div class="book" each={ book in booksData }>
				<button type="button" onclick={ likeBook }>LIKE</button> { book.title } by { book.author.user.last }
			</div>
		</div>
		<div class="col-md-6">
			<h3>Books You Like</h3>
			<div class="book" each={ book in myLikedBooksData }>
				<button type="button" onclick={ unlikeBook }>UNLIKE</button> { book.title } by { book.author.user.last } |
				<small><a class="whoLikes" onclick={ getUsers }>Who likes this?</a></small>
			</div>
		</div>
	</div>


	<script>
		var Book = Parse.Object.extend('Book');

		var that = this;

		this.books = [];
		this.booksData = [];

		this.myLikedBooks = [];
		this.myLikedBooksData = [];

		this.getUsers = function(event) {
			var bookData = event.item.book;
			var book = _.find(this.myLikedBooks, function(book) {
			  return bookData.objectId === book.id;
			});

			var relation = book.relation('userLikes');

			// Relations have a query() function that returns a Parse Query
			// for you to get the related objects
			var queryUsers = relation.query();

			queryUsers.find().then(function(users) {
				var userStr = "";

				_.each(users, function(user, index, list) {
				  userStr += user.get('username') + (index < list.length - 1 ? ", " : " ");
				});

				alert(userStr + 'like this book.');
			});
		};

		this.getBooks = function(){
		  var queryBooks = new Parse.Query(Book);
					queryBooks.include('author.user');

					// Technically, this is not efficient - slow compared to containedIn.
					// Why do you think?
					queryBooks.notContainedIn('userLikes', [Parse.User.current()]);

			queryBooks.find().then(function(books){
			  that.books = books;
				that.update();
			});
		};

		this.getMyLikedBooks = function(){
			var queryMyLikedBooks = new Parse.Query(Book);
					queryMyLikedBooks.include('author.user');

					// This constraint can be  used on Relation type data columns
					// It means, find all books where the list of users [ Parse.User.current(), userB, ...]
					// are contained in the 'userLikes' relationship.
					queryMyLikedBooks.containedIn('userLikes', [Parse.User.current()]);

					// For all intents and purposes, Parse.Relations are like special arrays.

			queryMyLikedBooks.find().then(function(books){
			  that.myLikedBooks = books;
				that.update();
			});
		};

		this.likeBook = function(event) {
			// Remember, we're working with copies of the book data...
			var bookData = event.item.book;

			// So once we get some data about the book, we can actually do a real
			// search for the actual Parse Book object in the this.books array.
			var book = _.find(this.books, function(book){
				return bookData.objectId === book.id;
			});

			// This .relation() function returns a relation object (see database)
			var relation = book.relation('userLikes');

			// We can add reference objects to the relation... in this case Users
			// who "like" the book.
			relation.add(Parse.User.current()); // relation will contain users (unique)

			book.save();

			this.books.remove(book);
			this.myLikedBooks.push(book);
		};

		this.unlikeBook = function(event) {
		  var bookData = event.item.book;
			var book = _.find(this.myLikedBooks, function(book) {
			  return bookData.objectId === book.id;
			});
			var relation = book.relation('userLikes');

			relation.remove(Parse.User.current());

			book.save();

			this.myLikedBooks.remove(book);
			this.books.push(book);
		};

		this.on('update', function(){
		  this.booksData = _.map(this.books, function(book){
				return book.toJSON();
			});

			this.myLikedBooksData = _.map(this.myLikedBooks, function(book){
				return book.toJSON();
			});
		});

		this.getBooks();
		this.getMyLikedBooks();

	</script>

	<style scoped>
		.book {
			margin-bottom: 10px;
		}
		.whoLikes {
			cursor: pointer;
		}
	</style>
</many-to-many-large>
