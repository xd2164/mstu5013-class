<one-to-one>
	<div class="page-header">
		<h3>ONE-TO-ONE <small>Pointers</small></h3>
	</div>

	<!-- <button onclick={ fetchAll }>Get ALL</button> -->

	<div class="row">
		<div class="col-md-6">
			<h4>USERS</h4>
			<div class="users">
				<div class="user" each={ user in users }>
					<strong>{ user.get('first') }</strong> <em>{ user.id }</em>
					<button onclick={ parent.makeAuthor }>Make Author</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<h4>AUTHORS</h4>
			<div class="authors">
				<div class="author" each={ author in authors }>
					<strong>{ author.get('user').get('first') }</strong> <em>{ author.get('user').id }</em>
					<button onclick={ parent.deleteAuthor }>Delete Author</button>
				</div>
			</div>
		</div>
	</div>


	<script>
		var Author = Parse.Object.extend('Author');
		var that = this;

		this.users = [];
		this.authors = [];

		// This function gets all users who are NOT authors
		this.getUsers = function() {
			// Query of Users
			var userQuery = new Parse.Query(Parse.User);
					userQuery.ascending('first');

			// Query of Authors
			var authorQuery = new Parse.Query('Author');

			// Constraint on User query, limiting only to Users whose objectId
			// does NOT match the userID property of Author object
			userQuery.doesNotMatchKeyInQuery('objectId', 'userID', authorQuery); // Kinda hack-y

			return userQuery.find().then(function(users) {
				that.users = users;
			});
		};

		this.getAuthors = function() {
			var authorQuery = new Parse.Query('Author');
					authorQuery.equalTo('editable', true); // Get only editable authors (superficial)
					authorQuery.include('user');

			return authorQuery.find().then(function(authors) {
				that.authors = authors;
			});
		};

		this.makeAuthor = function(event) {
			// Parse User object from event
		  var user = event.item.user;
			var author = new Author({
				// It's a bit redundant, but used for doesNotMatchKeyInQuery constraint later
				userID: user.id,
				user: user,

				// Only included as SUPERFICIAL protection to keep certain authors from being messed with by you'all
				editable: true,
				books: []
			});

			this.users.remove(user);
			this.authors.push(author);

			author.save();
		};

		this.deleteAuthor = function(event) {
			var author = event.item.author;

			this.authors.remove(author);
			this.users.push(author.get('user'));

			author.destroy();
		};

		this.fetchAll = function() {
			var promises = [
				this.getUsers(),
				this.getAuthors()
			];
			Parse.Promise.when(promises).then(function(){
			  that.update();
			});
		};

		// Each time, right before the update I sort the lists
		this.on('update', function() {
			// Note that I'm using underscore for sorting http://underscorejs.org/
			this.users = _.sortBy(this.users, function(user) {
			  return user.get('first');
			});
			this.authors = _.sortBy(this.authors, function(author) {
				return author.get('user').get('first');
			})
		});

		this.fetchAll();

	</script>

	<style scoped>
		.user, .author {
			margin-bottom: 10px;
		}
	</style>
</one-to-one>
