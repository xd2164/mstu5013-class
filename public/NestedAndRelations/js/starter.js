var Publisher = Parse.Object.extend('Publisher');
var Author = Parse.Object.extend('Author');
var Book = Parse.Object.extend('Book');

var p1 = new Publisher({
	name: 'Penguin Random House'
});
var p2 = new Publisher({
	name: 'Harper Collins'
});
var p3 = new Publisher({
	name: 'Hachette Livre'
});

Parse.Object.saveAll([p1,p2,p3]);

// Permanent Authors

var a = new Author({
  user: user,
  userID: user.id,
  editable: false
});
