Array.prototype.remove = function(item) {
  var index = this.indexOf(item);
	this.splice(index, 1);
};



Parse.initialize("JOiRQ4zeHDhhGbzr4oKw9QmKyrlpTcIH6k6uNzXY", "pnXpWoBz1Vx65MB38SNOVjcHnnQYG6L0jEpnVisJ");

var observer = riot.observable();

riot.mount('*');

riot.route('/one-to-one', function(){
  riot.mount('#demo', 'one-to-one');
});
riot.route('/one-to-many', function(){
  riot.mount('#demo', 'one-to-many');
});
riot.route('/many-to-many-small', function(){
  riot.mount('#demo', 'many-to-many-small');
});
riot.route('/many-to-many-large', function(){
  riot.mount('#demo', 'many-to-many-large');
});
riot.route(function(){
  riot.mount('#demo', 'one-to-one');
});

riot.route.start(true);
