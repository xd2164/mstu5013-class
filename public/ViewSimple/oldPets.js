// Since I already loaded the code for what PetView is, all I have to do
// is query Pets and build views and put them in the div#oldPets

var petQuery = new Parse.Query('Pet');

petQuery.find().then(function(pets) {

	// I'm using an underscore function which simplifies for loops
	_.each(pets, function(petModel) {

		var petView = new PetView({
			model: petModel
		});

		$('#oldPetViewer').append(petView.render().el);

	});

}, function(error) {
	alert(error.message);
});
