<login>
	
	<div class="login">
		<input type="text" id="username">
		<input type="password" id="password" onkeypress={ login }>
		<button onclick={ login }>Login</button>		
	</div>


	<script>
		var that = this;
		this.user = this.opts.user;

		// this.on('mount update unmount', function(eventName) {
		// 	console.log(eventName + ': login', this);
		// });

		this.login = function(event) {

			if (event.type === 'keypress' && event.which === 13 || event.type === 'click') {
				var username = this.username.value;
				var password = this.password.value;

				Parse.User.logIn(username, password).then(function(user) {
					pubsub.trigger('loggedIn', {
						data: 'DATAXXX'
					});
				});
			}

			return true;
		};
	</script>

	<style scoped>
		.login {
			position: absolute;
			background-color: yellow;
			top: 0;
			bottom: 0;
			width: 100%;
		}
	</style>

</login>