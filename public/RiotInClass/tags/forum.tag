<forum>

	<div class="forum">
<!-- 		<h1>{ magicNumber ? magicNumber : 'No Number' }</h1>
		<button onclick={ setMagicNumber }>Set Magic Number</button> -->

		<textarea id="textInput" placeholder="Enter post"></textarea>
		<button onclick={ createPost }>Create Post</button>

<!-- 		<input id="postTitle" type="text" placeholder="Enter title">
		<input name="postMessage" type="text" placeholder="Enter message">
		<button onclick={ createPost } >Create Post</button> -->



		<!-- Pass data to child. Data will be found like child.opts.postdata -->
		<!-- <post postdata={ opts }></post> -->

		<!-- Think of each THING in POSTS as a child tag -->
		<div each={ posts }>
			{ message }
			<!-- { console.log(this) } -->
			<button onclick={ deletePost }>Delete</button>
		</div>


	
		<!-- <post each={ posts }></post> -->

	</div>

	<script>
		var tag = this;

		// JS goes here
		// console.log('Forum Tag:', this);
		// console.log('Forum opts:', this.opts);

		this.posts = [];


		this.setMagicNumber = function(event) {
			this.magicNumber = Math.ceil(Math.random() * 10);
		};

		this.deletePost = function(event) {

			var targetPost = event.item;

			// var index = this.posts.indexOf(targetPost);
			// this.posts.splice(index, 1);

			this.posts.remove(targetPost);

			var Post = Parse.Object.extend('Post');
			var myPost = new Post();
				myPost.id = targetPost.id;

			myPost.destroy();


			// console.log(this, this.parent);
			console.log('EVENT', event);
		};

		this.jin = 'JIN KUWATA';

		// this.posts = this.opts.posts;

		this.createPost = function(event) {
			var post = {};
			post.message = this.textInput.value;
			this.posts.push(post);
			this.textInput.value = '';

			var Post = Parse.Object.extend('Post');
			var parsePost = new Post(post);
			parsePost.save().then(function(parseObj) {
				post.id = parseObj.id;
				console.log(tag.posts);
			});

			// var postObj = {
			// 	title: this.postTitle.value,
			// 	message: this.postMessage.value
			// };
			// this.posts.push(postObj);

			// this.postTitle.value = "";
			// this.postMessage.value = "";
		};

		// $('button').on('click', this.createPost);

	</script>

	<style scoped>
		/* Correction, apparently this is not a special Riot rule but an uncommon and new one in standard CSS, HTML */
		:scope { 
			display: block;
		};
		.forum {
			border: 1px solid orange;
			padding: 15px;
		}
	</style>

</forum>