<comment>
	<div class="comment">Comment Comment Comment</div>

	<script>
		console.log('Comment Tag:', this);
	</script>

	<style scoped>
		:scope {
			display: block;
		}
		.comment {
			border: 1px solid pink;
			padding: 15px;
			margin-bottom: 10px;
		}
	</style>
</comment>